int i;
void setup()
{
    //La led intégrée à la carte
    //pinMode(2, OUTPUT);
    //Les leds sur les ports correspondant
    pinMode(D0, OUTPUT);
    pinMode(D2, OUTPUT);
    pinMode(D4, OUTPUT);
    pinMode(D5, OUTPUT);
}

void loop()
{
    //On boucle 20 fois pour faire clignoter la LED
    for (i = 1; i <= 20; i++)
    {
        digitalWrite(D0, HIGH);
        delay(1000);
        digitalWrite(D0, LOW);
        digitalWrite(D2, HIGH);
        delay(1000);
        digitalWrite(D2, LOW);
        digitalWrite(D4, HIGH);
        delay(1000);
        digitalWrite(D4, LOW);
        digitalWrite(D5, HIGH);
        delay(1000);
        digitalWrite(D5, LOW);
    }
}